package main

import (
	// external
	"github.com/Ankr-network/go-ethereum-jsonrpc/jsonrpc"
	"go.mongodb.org/mongo-driver/mongo"

	// internal
	"codeberg.org/zhivtone/lib/helpers"
	"codeberg.org/zhivtone/zhivtone/internal/config"

	// stdlib
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
)

// config
var conf config.Config

// RPC server
var RPCserver *jsonrpc.Server

// database vatiables
var (
	dbClient *mongo.Client
	db       *mongo.Database
	dbUsers  *mongo.Collection
)

// logs
var (
	infoLog  *log.Logger = log.New(os.Stdout, " [  INF  ] ", log.Ldate|log.Ltime)
	errLog   *log.Logger = log.New(os.Stderr, " [ ERROR ] ", log.Ldate|log.Ltime|log.Llongfile)
	debugLog *log.Logger = log.New(io.Discard, " [ DEBUG ] ", log.Ldate|log.Ltime|log.Lshortfile)
)

// flag values
var (
	flagConfigPath string
	flagWriteDebug bool
)

func main() {
	defer infoLog.Println("HTTP-server stops serving...")
	if dbClient != nil {
		defer func() {
			dbClient.Disconnect(helpers.GetCtx())
		}()
	}

	http.HandleFunc("/ping", pingHandler)
	http.HandleFunc("/register", registerHandler)
	http.HandleFunc("/get_user", getUserHandler)
	http.HandleFunc("/alive", aliveHandler)

	http.HandleFunc("/add_actions", addActionsHandler)
	http.HandleFunc("/del_actions", delActionsHandler)
	http.HandleFunc("/edit_actions", editActionsHandler)

	http.HandleFunc("/settings", settingsHandler)
	http.HandleFunc("/types", typesHandler)
	http.HandleFunc("/", rootHandler)
	http.HandleFunc("/rpc", RPCserver.ServeHTTP)

	var err error

	if conf.API.UseTLS {
		infoLog.Println("HTTPS-server starts serving...")
		err = http.ListenAndServeTLS(
			conf.API.Domain+":"+strconv.Itoa(int(conf.API.Port)),
			conf.API.CertFile,
			conf.API.KeyFile,
			nil,
		)
	} else {
		infoLog.Println("HTTP-server starts serving...")
		err = http.ListenAndServe(
			conf.API.Domain+":"+strconv.Itoa(int(conf.API.Port)),
			nil,
		)
	}

	if err != nil {
		errLog.Printf("Fatal error: %v\n", err)
	}

}
