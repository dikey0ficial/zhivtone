package main

import (
	// external
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"

	// internal
	"codeberg.org/zhivtone/lib/execs"
	_ "codeberg.org/zhivtone/lib/execs/default_modules"
	"codeberg.org/zhivtone/lib/helpers"
	"codeberg.org/zhivtone/lib/types"

	// stdlib
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
)

func addActionsHandler(w http.ResponseWriter, r *http.Request) {
	if r == nil {
		return
	}

	w.Header().Add("Content-Type", "application/json")

	if r.Method != http.MethodPost {
		w.Header().Add("Allow", "POST")
		w.WriteHeader(405)
		w.Write([]byte(`{"error":"Method Not Allowed"}`))
		return
	}

	if r.Header.Get("Content-Type") != "application/json" {
		w.WriteHeader(415)
		w.Write([]byte(`{"error":"Unsupported Media Type"}`))
		return
	}

	login, pass, err := decryptCreds(r.Header.Get("Authorization"))
	if err != nil {
		w.Header().Add("WWW-Authenticate", "Basic")
		w.WriteHeader(401)
		w.Write([]byte(`{"error":"Unauthorized","msg":"` + err.Error() + `"}`))
		return
	}

	c, err := dbUsers.CountDocuments(
		helpers.GetCtx(),
		bson.M{
			"login": primitive.Regex{Pattern: "^" + login + "$", Options: "i"},
		},
	)

	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(`{"error":"Internal Server Error"}`))
		errLog.Printf("error accessing to db: %v\n", err)
		return
	}

	if c == 0 {
		w.WriteHeader(404)
		w.Write([]byte(`{"error":"Not Found","msg":"user by login not found"}`))
		return
	}

	var user types.User

	err = dbUsers.FindOne(
		helpers.GetCtx(),
		bson.M{
			"login": primitive.Regex{Pattern: "^" + login + "$", Options: "i"},
		},
	).Decode(&user)

	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(`{"error":"Internal Server Error"}`))
		errLog.Printf("error accessing to db: %v\n", err)
		return
	}

	var (
		passSum  = sha256.Sum256([]byte(pass))
		passHash = hex.EncodeToString(passSum[:])
	)

	if user.PasswordHash != passHash {
		w.Header().Add("WWW-Authenticate", "Basic")
		w.WriteHeader(401)
		w.Write([]byte(`{"error":"Unauthorized","msg":"` + err.Error() + `"}`))
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(`{"error":"Internal Server Error"}`))
		errLog.Printf("error reading body: %v\n", err)
		return
	}

	var acts []types.AutoAction

	err = json.Unmarshal(body, &acts)
	if err != nil {
		w.WriteHeader(400)
		w.Write([]byte(`{"error":"Bad Request","msg":"invalid json"}`))
		return
	}

	var push = bson.A{}

	for i, act := range acts {
		// zero or non-valid value checking
		if act.After == helpers.Duration(time.Duration(0)) {
			w.WriteHeader(400)
			w.Write([]byte(`{"error":"Bad Request","msg":"empty after in #` + strconv.Itoa(i) + ` action"}`))
			return
		} else if !isDurationAllowed(act.After) {
			w.WriteHeader(400)
			w.Write([]byte(`{"error":"Bad Request","msg":"non-valid after in #` + strconv.Itoa(i) + ` action"}`))
			return
		}

		if act.Executor == execs.ExecTypeEmpty {
			w.WriteHeader(400)
			w.Write([]byte(`{"error":"Bad Request","msg":"empty exec_type in #` + strconv.Itoa(i) + ` action"}`))
			return
		}

		if ex, ok := execs.ActionExecutors[act.Executor]; ok {
			valid, errtext := ex.CheckValid(act.Parameters)
			if !valid {
				w.WriteHeader(400)
				w.Write([]byte(`{"error":"Bad Request","msg":"invalid params in #` + strconv.Itoa(i) + ` action: ` + errtext + `"}`))
				return
			}
		}

		push = append(push, bson.M{
			"after":     act.After,
			"params":    act.Parameters,
			"exec_type": act.Executor,
		})
	}

	_, err = dbUsers.UpdateOne(
		helpers.GetCtx(),
		bson.M{
			"login": primitive.Regex{Pattern: "^" + login + "$", Options: "i"},
		},
		bson.M{
			"$push": bson.M{
				"settings.actions": bson.M{
					"$each": push,
				},
			},
		},
	)

	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(`{"error":"Internal Server Error"}`))
		errLog.Printf("error reading body: %v\n", err)
		return
	}

	w.WriteHeader(201)
	w.Write([]byte(`{"msg":"OK"}`))
}

func delActionsHandler(w http.ResponseWriter, r *http.Request) {
	if r == nil {
		return
	}

	w.Header().Add("Content-Type", "application/json")

	if r.Method != http.MethodPost {
		w.Header().Add("Allow", "POST")
		w.WriteHeader(405)
		w.Write([]byte(`{"error":"Method Not Allowed"}`))
		return
	}

	if r.Header.Get("Content-Type") != "application/json" {
		w.WriteHeader(415)
		w.Write([]byte(`{"error":"Unsupported Media Type"}`))
		return
	}

	login, pass, err := decryptCreds(r.Header.Get("Authorization"))
	if err != nil {
		w.Header().Add("WWW-Authenticate", "Basic")
		w.WriteHeader(401)
		w.Write([]byte(`{"error":"Unauthorized","msg":"` + err.Error() + `"}`))
		return
	}

	c, err := dbUsers.CountDocuments(
		helpers.GetCtx(),
		bson.M{
			"login": primitive.Regex{Pattern: "^" + login + "$", Options: "i"},
		},
	)

	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(`{"error":"Internal Server Error"}`))
		errLog.Printf("error accessing to db: %v\n", err)
		return
	}

	if c == 0 {
		w.WriteHeader(404)
		w.Write([]byte(`{"error":"Not Found","msg":"user by login not found"}`))
		return
	}

	var user types.User

	err = dbUsers.FindOne(
		helpers.GetCtx(),
		bson.M{
			"login": primitive.Regex{Pattern: "^" + login + "$", Options: "i"},
		},
	).Decode(&user)

	var (
		passSum  = sha256.Sum256([]byte(pass))
		passHash = hex.EncodeToString(passSum[:])
	)

	if passHash != user.PasswordHash {
		w.Header().Add("WWW-Authenticate", "Basic")
		w.WriteHeader(401)
		w.Write([]byte(`{"error":"Unauthorized","msg":"wrong password"}`))
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(`{"error":"Internal Server Error"}`))
		errLog.Printf("error reading body: %v\n", err)
		return
	}

	var delIdx = []int{}

	err = json.Unmarshal(body, &delIdx)
	if err != nil {
		w.WriteHeader(400)
		w.Write([]byte(`{"error":"Bad Request","msg":"invalid json"}`))
		return
	}

	// maybe not effective, but i needed
	// unique values and fast checks
	// is element in arr or not.
	var indexes = map[int]struct{}{}

	for _, idx := range delIdx {
		if idx > len(user.Actions)-1 {
			w.WriteHeader(400)
			w.Write([]byte(`{"error":"Bad Request","msg":"` + strconv.Itoa(idx) + ` > len(actions)"}`))
			return
		}
		indexes[idx] = struct{}{}
	}

	// because mongo somewhy
	// cannot pull by index
	var newActs = make([]types.AutoAction, 0)

	for i := range user.Actions {
		if _, ok := indexes[i]; !ok {
			newActs = append(newActs, user.Actions[i])
		}
	}

	_, err = dbUsers.UpdateOne(
		helpers.GetCtx(),
		bson.M{
			"login": primitive.Regex{Pattern: "^" + login + "$", Options: "i"},
		},
		bson.M{
			"$set": bson.M{
				"settings.actions": newActs,
			},
		},
	)

	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(`{"error":"Internal Server Error"}`))
		errLog.Printf("error accessing to db: %v\n", err)
		return
	}

	w.WriteHeader(200)
	w.Write([]byte(`{"msg":"OK"}`))
}

func editActionsHandler(w http.ResponseWriter, r *http.Request) {
	if r == nil {
		return
	}

	w.Header().Add("Content-Type", "application/json")

	if r.Method != http.MethodPost {
		w.Header().Add("Allow", "POST")
		w.WriteHeader(405)
		w.Write([]byte(`{"error":"Method Not Allowed"}`))
		return
	}

	if r.Header.Get("Content-Type") != "application/json" {
		w.WriteHeader(415)
		w.Write([]byte(`{"error":"Unsupported Media Type","msg":"only json"}`))
		return
	}

	login, pass, err := decryptCreds(r.Header.Get("Authorization"))
	if err != nil {
		w.Header().Add("WWW-Authenticate", "Basic")
		w.WriteHeader(401)
		w.Write([]byte(`{"error":"Unauthorized","msg":"` + err.Error() + `"}`))
		return
	}

	c, err := dbUsers.CountDocuments(
		helpers.GetCtx(),
		bson.M{
			"login": primitive.Regex{Pattern: "^" + login + "$", Options: "i"},
		},
	)

	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(`{"error":"Internal Server Error"}`))
		errLog.Printf("error accessing to server: %v\n", err)
		return
	}

	if c == 0 {
		w.WriteHeader(404)
		w.Write([]byte(`{"error":"Not Found","msg":"user by login not found"}`))
		return
	}

	var user types.User

	err = dbUsers.FindOne(
		helpers.GetCtx(),
		bson.M{
			"login": primitive.Regex{Pattern: "^" + login + "$", Options: "i"},
		},
	).Decode(&user)

	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(`{"error":"Internal Server Error"}`))
		errLog.Printf("error accessing to db: %v\n", err)
		return
	}

	var (
		passSum  = sha256.Sum256([]byte(pass))
		passHash = hex.EncodeToString(passSum[:])
	)

	if passHash != user.PasswordHash {
		w.Header().Add("WWW-Authenticate", "Basic")
		w.WriteHeader(401)
		w.Write([]byte(`{"error":"Unauthorized","msg":"wrong password"}`))
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(`{"error":"Internal Server Error"}`))
		errLog.Printf("error reading body: %v\n", err)
		return
	}

	var edActs map[int]types.AutoAction

	err = json.Unmarshal(body, &edActs)

	if err != nil {
		w.WriteHeader(400)
		w.Write([]byte(`{"error":"Bad Request","msg":"invalid json"}`))
		debugLog.Printf("error parsing json: %v\n", err)
		return
	}

	// because mongodb cannot realise
	// normal accessing by index for array
	var resultActs = user.Actions

	for idx, act := range edActs {
		if idx > len(resultActs)-1 {
			w.WriteHeader(400)
			w.Write([]byte(`{"error":"Bad Request","msg":"` + strconv.Itoa(idx) + ` > len(actions)"}`))
			return
		}

		if act.After != helpers.Duration(time.Duration(0)) {
			if !isDurationAllowed(act.After) {
				w.WriteHeader(400)
				w.Write([]byte(`{"error":"Bad Request","msg":"non-valid after in #` + strconv.Itoa(idx) + ` action"}`))
				return
			}

			resultActs[idx].After = act.After
		}

		if act.Parameters != nil {
			if ex, ok := execs.ActionExecutors[act.Executor]; ok {
				valid, errtext := ex.CheckValid(act.Parameters)
				if !valid {
					w.WriteHeader(400)
					w.Write([]byte(`{"error":"Bad Request","msg":"invalid params in #` + strconv.Itoa(idx) + ` action: ` + errtext + `"}`))
					return
				}
			}
			resultActs[idx].Parameters = act.Parameters
		}

		if act.Executor != execs.ExecTypeEmpty {
			resultActs[idx].Executor = act.Executor
		}
	}
	_, err = dbUsers.UpdateOne(
		helpers.GetCtx(),
		bson.M{
			"login": primitive.Regex{Pattern: "^" + login + "$", Options: "i"},
		},
		bson.M{
			"$set": bson.M{
				"settings.actions": resultActs,
			},
		},
	)

	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(`{"error"::"Internal Server Error"}`))
		errLog.Printf("error accessing to db: %v\n", err)
		return
	}

	w.WriteHeader(200)
	w.Write([]byte(`{"msg":"OK"}`))
}
