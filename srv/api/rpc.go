package main

import (
	//internal
	// . "codeberg.org/zhivtone/lib/types/rpc-types"

	//stdlib
	"time"
)

type Ping struct{}

type PingArgs struct {
	Message string `json:"msg"`
}

type PingReply struct {
	Message string    `json:"msg"`
	Time    time.Time `json:"time"`
	IsPing  bool      `json:"is_ping"`
}

func (p *Ping) Ping(args PingArgs) (PingReply, error) {
	return PingReply{
		Message: "Pong",
		Time:    time.Now().UTC(),
		IsPing:  args.Message == "Ping",
	}, nil
}
