package main

import (
	// external
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"

	// internal
	"codeberg.org/zhivtone/lib/helpers"
	"codeberg.org/zhivtone/lib/types"
	"codeberg.org/zhivtone/zhivtone/internal/config"

	// stdlib
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"time"
)

// config
var conf config.Config

// database vatiables
var (
	dbClient *mongo.Client
	db       *mongo.Database
	dbUsers  *mongo.Collection
)

// logs
var (
	infoLog  *log.Logger = log.New(os.Stderr, " [  INF  ] ", log.Ldate|log.Ltime)
	errLog   *log.Logger = log.New(os.Stderr, " [ ERROR ] ", log.Ldate|log.Ltime|log.Llongfile)
	debugLog *log.Logger = log.New(io.Discard, " [ DEBUG ] ", log.Ldate|log.Ltime|log.Lshortfile)
)

// flag values
var (
	flagConfigPath string
	flagWriteDebug bool
)

func main() {
	os.Exit(mainWithCode())
}

// because of defer
func mainWithCode() int {
	if dbClient != nil {
		defer func() {
			dbClient.Disconnect(helpers.GetCtx())
		}()
	}

	cur, err := dbUsers.Find(helpers.GetCtx(), bson.M{})
	if err != nil {
		errLog.Printf("Error accessing to db: %v\n", err)
		return 1
	}

	var (
		user    types.User
		expired []int
	)

	fmt.Println("{")

	for cur.Next(helpers.GetCtx()) {
		if cur.Err() != nil {
			errLog.Printf("cursor error: %v\n", cur.Err())
			continue
		}

		err = cur.Decode(&user)
		if err != nil {
			errLog.Printf("error decoding user: %v\n", err)
			continue
		}

		if expired != nil {
			fmt.Println(",")
		}

		expired = []int{}

		for i, act := range user.Actions {
			if time.Now().Sub(user.LastBeat) > time.Duration(act.After) && !act.Executed {
				expired = append(expired, i)
			}
		}

		if len(expired) > 0 {
			b, err := json.Marshal(expired)
			if err != nil {
				errLog.Printf("error marshalling user: %v\n", err)
				continue
			}
			fmt.Printf("\"%s\": %s", user.Login, string(b))
		}
	}

	fmt.Print("\n}\n")
	return 0
}
