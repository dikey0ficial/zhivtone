package config

type Config struct {
	DatabaseURL string `toml:"db_url"`

	API struct {
		Domain   string `toml:"domain"`
		Port     uint16 `toml:"port"`
		UseTLS   bool   `toml:"use_tls"`
		CertFile string `toml:"cert_file"`
		KeyFile  string `toml:"key_file"`
	} `toml:"api"`

	Settings struct {
		CloseRegistration bool `toml:"close_registration" json:"close_registration"`
	} `toml:"settings" json:"settings"`
}
